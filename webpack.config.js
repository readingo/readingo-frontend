/* eslint-disable */
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserJSPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

const SRC_DIR = path.resolve(__dirname, 'src');
const DIST_DIR = path.resolve(__dirname, 'dist');

const devMode = process.env.NODE_ENV !== 'production';

module.exports = {
	optimization: {
		minimizer: [
			new TerserJSPlugin({}),
			new OptimizeCSSAssetsPlugin({
				cssProcessorPluginOptions: {
					preset: [
						'default',
						{
							discardComments: { removeAll: true },
						},
					],
				},
			})
		],
	},
	entry: SRC_DIR + '/index.js',
	output: {
		path: DIST_DIR,
		publicPath: '/',
		filename: '[name].[contenthash].js'
	},
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				include: SRC_DIR,
				use: ['babel-loader']
			},
			{
				test: /\.module\.scss$/,
				loader: [
					devMode ? 'style-loader' : MiniCssExtractPlugin.loader,
					{
						loader: 'css-loader',
						options: {
							modules: true,
							localsConvention: 'camelCase'
						}
					},
					'sass-loader'
				]
			},
			{
				test: /\.scss$/,
				exclude: /\.module.(scss)$/,
				use: [
					devMode ? 'style-loader' : MiniCssExtractPlugin.loader,
					'css-loader',
					'sass-loader',
				],
			},
			{
				test: /\.css$/,
				use: [
					devMode ? 'style-loader' : MiniCssExtractPlugin.loader,
					'css-loader'
				],
			},
			{
				test: /\.(png|jpeg|jpg|gif|svg|webp)$/,
				use: [
					'file-loader'
				],
			},
			{
				test: /\.(woff|woff2|eot|ttf|otf)$/,
				use: [
					'file-loader'
				],
			},
		]
	},
	resolve: {
		extensions: ['*', '.js', '.jsx']
	},
	devServer: {
		contentBase: DIST_DIR,
		historyApiFallback: true,
		port: 9000,
		proxy: {
			'/api/book': {
				target: 'http://localhost:3000',
				pathRewrite: { '^/api/book': '' }
			},
			'/api/community': {
				target: 'http://localhost:9002',
				pathRewrite: { '^/api/community': '' }
			}
		}
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: 'src/index.html'
		}),
		new MiniCssExtractPlugin({
			filename: devMode ? '[name].css' : '[name].[hash].css',
			chunkFilename: devMode ? '[id].css' : '[id].[hash].css',
		}),
	]
};
