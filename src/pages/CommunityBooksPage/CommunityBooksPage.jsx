import React, { useEffect } from 'react';
import { Card } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

import 'semantic-ui-css/components/card.min.css';
import { useGlobalState } from '../../store/global.store';
import { LOAD_BOOKS } from '../../store/book.store';
import { kyX } from '../../utils/extendedKy';

export default () => {
	const [{ book }, dispatch] = useGlobalState();

	useEffect(() => {
		kyX.get('/api/book').json().then(payload =>
			dispatch({
				type: LOAD_BOOKS,
				payload
			})
		);
	}, []);


	return (
		<Card.Group itemsPerRow={ 5 }>
			{ book.map(book => (
				<Card
					image={ book.thumbnail }
					header={ book.title }
					meta={ book.author }
					description={ book.description }
					key={ book._id }
					extra={ book.pageCount + ' pages' }
					link
					as={ Link }
					to={ `/read/${ book._id }` }
				/>
			))
			}
		</Card.Group>
	);
};
