import React, { useState } from 'react';
import { Button, Form, Segment } from 'semantic-ui-react';

import Logo from '../../assets/img/logo-green-medium.svg';

import classes from './LoginPage.module.scss';

import 'semantic-ui-css/components/button.min.css';
import 'semantic-ui-css/components/form.min.css';
import 'semantic-ui-css/components/segment.min.css';
import 'semantic-ui-css/components/input.min.css';
import ky from 'ky';
import { useGlobalState } from '../../store/global.store';
import { USER_LOGIN } from '../../store/user.store';
import { Redirect } from 'react-router-dom';

export default () => {
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [{ user }, dispatch] = useGlobalState();

	const onLogin = async () => {
		try {
			const { token, user } = await ky.post('/api/community/user/login', { json: { email, password } }).json();
			localStorage.setItem('accessToken', token);
			dispatch({
				type: USER_LOGIN,
				payload: user,
			});
		} catch (e) {
			console.log('Error', (await e.response.json()));
		}
	};

	return (
		<Segment className={ classes.container }>
			{
				user.loggedIn && <Redirect to={ '/community' }/>
			}
			<Form>
				<img src={ Logo } alt="Readingo" className={ classes.logo }/>
				<Form.Input fluid label='Email' placeholder='Email' onChange={ (_, { value }) => setEmail(value) }/>
				<Form.Input fluid label='Password' placeholder='Password' type='password' onChange={ (_, { value }) => setPassword(value) }/>
				<div className={ classes.buttons }>
					<Button fluid color='green' onClick={ () => onLogin() }>Login</Button>
					<Button fluid color='gray'>Register</Button>
				</div>
			</Form>
		</Segment>
	);
};
