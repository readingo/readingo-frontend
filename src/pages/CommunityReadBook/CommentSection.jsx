import React, { useEffect, useState } from 'react';
import classes from './CommunityReadBook.module.scss';
import { StickyCommentHeader } from './StickyCommentHeader';
import { Divider, Feed, Message, Segment } from 'semantic-ui-react';
import { PageComment } from './PageComment';
import { kyX } from '../../utils/extendedKy';

export const CommentSection = ({ id, page, pages }) => {
	const [pageComments, setPageComments] = useState([]);

	useEffect(() => {
		setPageComments([...new Array(pages).keys()].map(() => []));
	}, []);

	useEffect(() => {
		loadComments().catch();
	}, [id, page]);

	const loadComments = async () => {
		const comments = await kyX.get(`/api/book/${ id }/comment/${ page }`).json();
		setPageComments(c => {
			const x = [...c];
			x[page] = comments.comments;
			return x;
		});
	};

	const addComment = async p => {
		const input = document.querySelector(`#addComment${ p }`);
		const comment = { postedBy: 'me', contents: input.value, page: p };
		input.value = '';

		const commentInserted = await kyX.post(`/api/book/${ id }/comment`,
			{
				json: comment
			}
		).json();

		setPageComments(c => {
			const x = [...c];
			x[p].push(commentInserted);
			return x;
		});

	};

	return (
		<Segment className={ classes.sideComments }>
			<StickyCommentHeader className={ classes.stickyTop } addComment={ addComment } i={ page }/>
			<Feed>
				{
					pageComments?.[page]?.map(c =>
						c &&
						<PageComment
							name={ c.postedBy }
							comment={ c.contents }
							time={ new Date(c.postedAt).toLocaleString(undefined, { timeStyle: 'short', dateStyle: 'medium' }) }
							likes={ 10 }
							profileImage={ 'https://api.adorable.io/avatars/28/' + c.postedBy }
							key={ c._id }
						/>
					)
				}
				{
					pageComments?.[page]?.length === 0 &&
					<React.Fragment>
						<Message size={ 'mini' }>No comments</Message>
						<Divider hidden/>
					</React.Fragment>
				}
			</Feed>
		</Segment>
	);
};
