import React from 'react';
import { Segment } from 'semantic-ui-react';
import classes from './CommunityReadBook.module.scss';
import { Page } from 'react-pdf';
import { CommentSection } from './CommentSection';

export const DocumentPage = ({ pageIndex, pageCount, id }) => {
	return (
		<React.Fragment>
			<div>
				{/*{ rangeTo(Math.random() * 10, 10).map(pageIndex => (
								<Label basic pointing='right' as='a' className={ classes.tag }>
									<Image avatar className={ classes.tagImage } src={ 'https://api.adorable.io/avatars/17/sdcdsvhbr' + Math.round(Math.random() * 100) }/>
								</Label>
							))
							*/ }
			</div>
			<Segment raised className={ classes.page }>
				<Page pageNumber={ pageIndex } renderAnnotationLayer={ false } renderTextLayer={ false } scale={ 1 }/>
			</Segment>
			<CommentSection page={ pageIndex } pages={ pageCount } id={ id }/>
		</React.Fragment>
	);
};
