import React from 'react';
import classes from './CommunityReadBook.module.scss';
import { Divider, Input } from 'semantic-ui-react';

import 'semantic-ui-css/components/divider.min.css';
import 'semantic-ui-css/components/input.min.css';
import 'semantic-ui-css/components/icon.min.css';

const keyDownHandler = async (event, page, addComment) => {
	if (event.key === 'Enter') {
		await addComment(page);
	}
};

export const StickyCommentHeader = ({addComment, i}) => {
	return (
		<div className={ classes.stickyTop }>
			<Input
				id={ `addComment${ i }` }
				fluid
				action={ { icon: 'send', onClick: () => addComment(i) } }
				placeholder='Add comment...'
				onKeyDown={ e => keyDownHandler(e, i, addComment) }
			/>
			<Divider hidden/>
		</div>
	);
};
