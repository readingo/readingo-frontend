import React from 'react';
import { Document } from 'react-pdf/dist/entry.webpack';
import { IntersectionBoundary } from '../../shared/IntersectionBoundary/IntersectionBoundary';
import classes from './CommunityReadBook.module.scss';
import { Segment } from 'semantic-ui-react';
import { useParams } from 'react-router-dom';

import 'semantic-ui-css/components/divider.min.css';
import 'semantic-ui-css/components/feed.min.css';
import 'semantic-ui-css/components/input.min.css';
import 'semantic-ui-css/components/segment.min.css';
import 'semantic-ui-css/components/message.min.css';

import { Loader } from '../../shared/Loader/Loader';
import { processPdf } from '../../utils/processPDF';
import { useGlobalState } from '../../store/global.store';
import { READ_DOCUMENT_LOADED, READ_HIDE_PAGE, READ_SHOW_PAGE } from '../../store/read.store';
import { rangeTo } from '../../utils/rangeTo';
import { DocumentPage } from './DocumentPage';

export default () => {
	const { file } = useParams({});
	const [{ read }, dispatch] = useGlobalState();

	const onPageShown = i => {
		if (i < read?.pageCount && !(read?.pages?.includes(i) ?? true)) {
			dispatch({
				type: READ_SHOW_PAGE,
				payload: i
			});
		}
	};

	const onPageHide = i => {
		if (i < read?.pageCount && (read?.pages?.includes(i) ?? false)) {
			dispatch({
				type: READ_HIDE_PAGE,
				payload: i
			});
		}
	};

	const onDocumentLoaded = async (pdf) => {
		const processed = await processPdf(pdf);
		dispatch({
			type: READ_DOCUMENT_LOADED,
			payload: processed
		});
	};


	const onLoadError = (...args) => {
		console.log('Load error', args);
	};

	return <Document
		file={ `/${ file }.pdf` }
		onLoadSuccess={ onDocumentLoaded }
		onLoadError={ onLoadError }
		className={ classes.document }
		renderAnnotationLayer={ false }
		loading={ <Loader/> }
		options={ {
			cMapUrl: '//cdn.jsdelivr.net/npm/pdfjs-dist@2.1.266/cmaps/',
			cMapPacked: true,
		} }
	>
		{ rangeTo(1, read?.pageCount)?.map(pageIndex =>
			<React.Fragment key={ pageIndex }>
				<IntersectionBoundary onEnter={ () => onPageShown(pageIndex) } onExit={ () => onPageHide(pageIndex) } rootMargin={ read?.pageHeight[pageIndex - 1] + 'px' }/>
				{
					(read?.pages.includes(pageIndex) ?? false) &&
					<DocumentPage id={ file } pageCount={ read?.pageCount } pageIndex={ pageIndex }/>
				}
				{
					!read?.pages.includes(pageIndex) &&
					<React.Fragment>
						<div/>
						<Segment raised className={ classes.placeholder } style={ { height: read?.pageHeight[pageIndex - 1] } }>
							<Loader/>
						</Segment>
						<div/>
					</React.Fragment>
				}
			</React.Fragment>
		) }
	</Document>;
};
