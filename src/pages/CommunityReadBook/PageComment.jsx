import React from 'react';
import { Feed, Icon } from 'semantic-ui-react';

export const PageComment = ({ profileImage, name, time, comment, likes }) => {
	return (
		<Feed.Event>
			<Feed.Label>
				<img src={ profileImage } alt={ 'Profile image' }/>
			</Feed.Label>
			<Feed.Content>
				<Feed.Summary>
					<Feed.User>{ name }</Feed.User>
					<Feed.Date>{ time }</Feed.Date>
				</Feed.Summary>
				<Feed.Extra text>
					{ comment }
				</Feed.Extra>
				<Feed.Meta>
					<Feed.Like>
						<Icon name='like'/>{ likes } Likes
					</Feed.Like>
				</Feed.Meta>
			</Feed.Content>
		</Feed.Event>
	);
};
