import React, { useEffect } from 'react';
import { Divider, Image, List } from 'semantic-ui-react';
import 'semantic-ui-css/components/divider.css';
import 'semantic-ui-css/components/list.min.css';
import 'semantic-ui-css/components/image.min.css';
import { useGlobalState } from '../../store/global.store';
import { kyX } from '../../utils/extendedKy';
import { LOAD_COMMUNITY } from '../../store/user.store';

export default () => {
	const [{ user }, dispatch] = useGlobalState();

	useEffect(() => {
		kyX.get(`/api/community/${ user.community.id }`).json().then(
			community => {
				dispatch({
					type: LOAD_COMMUNITY,
					payload: community
				});
			}
		);
	}, []);

	return (
		<React.Fragment>
			<h1>{ user?.community?.name }</h1>
			<p>{ user?.community?.description }</p>
			<Divider/>
			<h3>Members</h3>
			<List divided relaxed>
				{ user?.community?.members?.map(user => (
					<List.Item key={ user.id }>
						<Image avatar src={ 'https://api.adorable.io/avatars/28/' + user.id }/>
						<List.Content>
							<List.Header as='a'>{ user.username }</List.Header>
							<List.Description>
								{ user.email }
							</List.Description>
						</List.Content>
					</List.Item>
				))
				}
			</List>
		</React.Fragment>
	);
};
