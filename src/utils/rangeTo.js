export const rangeTo = (from, to) =>
	from && to && from !== to ? [...Array(Math.round(to) - Math.round(from) + 1).keys()].map(x => x + Math.round(from)) : undefined;
