export const repeatOptimizer = (array = []) => {
	let prev = array[-1];
	let result = [];
	array.forEach(value => {
		if (value !== prev) {
			result.push({ value: value, count: 1 });
		} else {
			result[result.length - 1].count++;
		}
		prev = value;
	});
	return result.map(v => `repeat(${ v.count }, 16px ${ v.value }px) `).join();
};
