import { kyX } from './extendedKy';
import { USER_HYDRATE_FAILED, USER_LOGIN } from '../store/user.store';

const tokenToId = (token) => {
	const tokenPayload = token.split('.')[1];
	const base64 = atob(tokenPayload);
	const parsed = JSON.parse(base64);
	return parsed.aud;
};

export const hydrateUser = async (dispatch) => {
	const token = localStorage.getItem('accessToken');
	if (token !== null) {
		const user = await kyX.get(`/api/community/user/${ tokenToId(token) }`).json();
		dispatch({
			type: USER_LOGIN,
			payload: user
		});
	} else {
		dispatch({ type: USER_HYDRATE_FAILED });
	}
};
