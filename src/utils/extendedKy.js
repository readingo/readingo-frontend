import ky from 'ky';

export const kyX = ky.extend({
	hooks: {
		beforeRequest: [
			(_, { headers }) => {
				headers.set('Authorization', localStorage.getItem('accessToken') ?? '');
			}
		]
	}
});
