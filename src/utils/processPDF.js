import classes from '../pages/CommunityReadBook/CommunityReadBook.module.scss';
import { repeatOptimizer } from './repeatOptimizer';

export const processPdf = async (pdf) => {
	const heights = [];
	for (let i = 1; i <= pdf.numPages; i++) {
		let page = await pdf.getPage(i);
		let viewport = page.getViewport({ scale: 1 });
		heights.push(viewport.height);
	}

	document.querySelector(`.${ classes.document }`).style.height = repeatOptimizer(heights);

	return {
		pageCount: pdf.numPages,
		pages: [1, 2, 3],
		pageHeight: heights,
	};
};
