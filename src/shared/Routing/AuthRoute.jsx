import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { useGlobalState } from '../../store/global.store';
import { Loader } from '../Loader/Loader';

export const AuthRoute = ({ children, ...rest }) => {
	const [{ user }] = useGlobalState();
	return (
		<Route
			{ ...rest }
			render={ ({ location }) => {
				return (
					<React.Fragment>
						{ user.isHydrating ? <Loader/> :
							<React.Fragment>
								{
									user.loggedIn ? children : (
										<Redirect
											to={ {
												pathname: '/',
												state: { from: location }
											} }
										/>
									)
								}
							</React.Fragment>
						}
					</React.Fragment>
				);
			}
			}
		/>
	);
};
