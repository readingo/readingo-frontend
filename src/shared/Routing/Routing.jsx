import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { Loader } from '../Loader/Loader';
import loadable from '@loadable/component';
import { AuthRoute } from './AuthRoute';

const fallback = { fallback: <Loader/> };
const LazyCommunityDetailsPage = loadable(() => import('../../pages/CommunityDetailsPage/CommunityDetailsPage'), fallback);
const LazyCommunityBooksPage = loadable(() => import('../../pages/CommunityBooksPage/CommunityBooksPage'), fallback);
const LazyCommunityReadBook = loadable(() => import('../../pages/CommunityReadBook/CommunityReadBook'), fallback);
const LazyLoginPage = loadable(() => import('../../pages/LoginPage/LoginPage'), fallback);

export const Routing = () => {
	return (
		<Switch>
			<AuthRoute path='/community'><LazyCommunityDetailsPage/></AuthRoute>
			<AuthRoute path='/books'><LazyCommunityBooksPage/></AuthRoute>
			<AuthRoute path='/read/:file'><LazyCommunityReadBook/></AuthRoute>
			<Route path='/' exact component={ LazyLoginPage }/>
		</Switch>
	);
};
