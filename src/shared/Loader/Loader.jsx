import React from 'react';
import { Dimmer, Loader as L } from 'semantic-ui-react';

import 'semantic-ui-css/components/dimmer.min.css';
import 'semantic-ui-css/components/loader.min.css';

export const Loader = ({ text = 'Loading' }) => {
	return (
		<Dimmer active><L size='mini'>{ text }</L></Dimmer>
	);
};
