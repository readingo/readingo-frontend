import React, { useEffect } from 'react';
import { Container } from 'semantic-ui-react';
import { TopBar } from '../TopBar/TopBar';
import classes from '../../App.module.scss';
import { Routing } from '../Routing/Routing';
import 'semantic-ui-css/components/container.min.css';
import { hydrateUser } from '../../utils/hydrateUser';
import { useGlobalState } from '../../store/global.store';

export const MainLayout = () => {
	const [, dispatch] = useGlobalState();
	useEffect(() => {
		hydrateUser(dispatch).catch();
	}, []);
	return (
		<React.Fragment>
			<TopBar/>
			<Container className={ classes.container }>
				<Routing/>
			</Container>
		</React.Fragment>
	);
};
