import React from 'react';
import { NavLink } from 'react-router-dom';

import { Menu } from 'semantic-ui-react';
import classes from './TopBar.module.scss';

import Logo from '../../assets/img/logo-green-small.svg';
import 'semantic-ui-css/components/menu.min.css';
import { useGlobalState } from '../../store/global.store';

export const TopBar = () => {
	const [{ user }] = useGlobalState();
	return (
		<Menu fixed='top' inverted color='green'>
			<Menu.Item header>
				<img src={ Logo } className={ classes.logo } alt={ 'Readingo logo' }/>
				<span className={ classes.brandName }> Readingo </span>
			</Menu.Item>
			{ user.loggedIn &&
			<React.Fragment>
				<Menu.Item activeClassName={ 'active' } as={ NavLink } to='/community'>
					Community
				</Menu.Item>
				< Menu.Item activeClassName={ 'active' } as={ NavLink } to='/books'>
					Books
				</Menu.Item>
			</React.Fragment>
			}
		</Menu>
	);
};
