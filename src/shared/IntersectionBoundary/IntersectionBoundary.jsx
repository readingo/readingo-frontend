import React, { createRef, useEffect } from 'react';
import classes from './IntersectionBoundary.module.scss';

export const IntersectionBoundary = ({ onEnter, onExit, rootMargin }) => {
	const ref = createRef();
	useEffect(() => {
		const observer = new IntersectionObserver(entries => {
			if (entries[0].intersectionRatio > 0) {
				onEnter();
			} else {
				onExit();
			}
		}, { root: document.body, rootMargin: (rootMargin ?? 100), threshold: [0] });

		observer.observe(ref.current);

		return () => {
			observer.disconnect();
		};
	});
	return (
		<div ref={ ref } className={ classes.boundary }/>
	);
};
