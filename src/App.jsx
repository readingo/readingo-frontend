import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

import { GlobalStateProvider } from './store/global.store';
import { MainLayout } from './shared/MainLayout/MainLayout';

import 'semantic-ui-css/components/reset.min.css';
import 'semantic-ui-css/components/site.min.css';


export const App = () => {
	return (
		<GlobalStateProvider>
			<Router>
				<MainLayout/>
			</Router>
		</GlobalStateProvider>
	);
};
