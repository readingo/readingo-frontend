export const USER_LOGIN = 'USER_LOGIN';
export const USER_LOGOUT = 'USER_LOGIN';
export const LOAD_COMMUNITY = 'LOAD_COMMUNITY';
export const USER_HYDRATE_FAILED = 'USER_HYDRATE_FAILED';

export const userStoreInitialState = {
	loggedIn: false,
	isHydrating: true,
	id: '',
	username: '',
	email: '',
	community: {
		id: '',
		name: '',
		description: ''
	}
};

export const userStoreReducer = (state, action) => {
	switch (action.type) {
	case USER_LOGIN:
		return { ...action.payload, loggedIn: true, isHydrating: false };
	case USER_LOGOUT:
		return { ...userStoreInitialState, isHydrating: false };
	case LOAD_COMMUNITY:
		return { ...state, community: action.payload };
	case USER_HYDRATE_FAILED:
		return { ...state, isHydrating: false };
	default:
		return state;
	}
};
