export const LOAD_BOOKS = 'LOAD_BOOKS';
export const ADD_BOOK = 'ADD_BOOK';

export const bookStoreInitialState = [];

export const bookStoreReducer = (state, action) => {
	switch (action.type) {
	case LOAD_BOOKS:
		return [...action.payload];
	case ADD_BOOK:
		return [...state, ...action.payload];
	default:
		return state;
	}
};
