import React, { createContext, useContext, useReducer } from 'react';
import { bookStoreInitialState, bookStoreReducer } from './book.store';
import { readStoreInitialState, readStoreReducer } from './read.store';
import { userStoreInitialState, userStoreReducer } from './user.store';

export const GlobalStateContext = createContext();

const mainReducer = ({ book, read, user }, action) => ({
	book: bookStoreReducer(book, action),
	read: readStoreReducer(read, action),
	user: userStoreReducer(user, action)
});

const mainInitialState = {
	book: bookStoreInitialState,
	read: readStoreInitialState,
	user: userStoreInitialState
};

export const GlobalStateProvider = ({ children }) =>
	<GlobalStateContext.Provider value={ useReducer(mainReducer, mainInitialState) }>
		{ children }
	</GlobalStateContext.Provider>;

export const useGlobalState = () => useContext(GlobalStateContext);
