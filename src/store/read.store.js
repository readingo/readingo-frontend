export const READ_BOOK_OPENED = 'READ_BOOK_OPENED';
export const READ_DOCUMENT_LOADED = 'READ_DOCUMENT_LOADED';
export const READ_SHOW_PAGE = 'READ_SHOW_PAGE';
export const READ_HIDE_PAGE = 'READ_HIDE_PAGE';

export const readStoreInitialState = {
	book: {},
	pageCount: 0,
	pages: [],
	pageHeight: [],
};

export const readStoreReducer = (state, action) => {
	switch (action.type) {
	case READ_BOOK_OPENED:
		return { ...state, book: action.payload };
	case READ_DOCUMENT_LOADED:
		return { ...state, ...action.payload };
	case READ_SHOW_PAGE:
		return { ...state, pages: [...state.pages, action.payload] };
	case READ_HIDE_PAGE:
		return { ...state, pages: state.pages.filter(i => i !== action.payload) };
	default:
		return state;
	}
};
