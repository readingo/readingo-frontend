import React from 'react';
import { render } from 'react-dom';
import './style/_overrides.scss';
import { App } from './App';

const rootElement = document.getElementById('root');
render(<App />, rootElement);
