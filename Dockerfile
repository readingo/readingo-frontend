FROM nginx:1.17.5-alpine
EXPOSE 9000
COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY /dist /usr/share/nginx/html
